let podajIdWypozyczenia = 0;
let podajIdPojazdu = 0;
let podajIdPracownika = 0;
const DOMtable = document.getElementById("wypozyczenia");
document.addEventListener("DOMContentLoaded", (event) => {
  document.getElementsByClassName("container-fluid")[0].classList.add("loader");


  zapytanie(urlListaCzynnosciEksploatacyjnych, wczytajListeCzynnosciEksploatacyjnych);
  zapytanie(urlStatusyWypozyczenia, wczytajStatusyWypozyczenia);
  if(localStorage.getItem('rola') === "kierowca"){
    zapytanie(urlWypozyczenia+'/?id_pracownika='+localStorage.getItem('id_pracownika'), wczytajWypozyczenia);

  }else {
  zapytanie(urlWypozyczenia, wczytajWypozyczenia);
  }
  document.getElementById("wypozycz-pojazd-btn").addEventListener("click", () => {
    if(zbierzDaneDoWypozyczeniaPojazdu()){
    updateDoBazy(zbierzDaneDoWypozyczeniaPojazdu(), urlWypozyczenia, podajIdWypozyczenia);
    setTimeout(() => {
     window.location.reload(true);

    }, 500);
  }
  });
  document.getElementById("zakoncz-wypozyczenie-btn").addEventListener("click", () => {
    if(zbierzDaneDoZakonczeniaWypozyczeniaPojazdu()){
    updateDoBazy(zbierzDaneDoZakonczeniaWypozyczeniaPojazdu(), urlWypozyczenia, podajIdWypozyczenia);
    setTimeout(() => {
      //  window.location.reload(true);
    //  postDoBazy(urlCzynnosciEksploatacyjnych,zbierzDaneCzynnosciEksploatacyjnych());
    zbierzDaneCzynnosciEksploatacyjnych();
    }, 200);
    setTimeout(() => {
     window.location.reload(true);
    }, 1200);
  }
  });


});
let ile = 0;
const dodajPoleWyboruCzynnosciEksploatacyjnej = (id) => {
  ile = ile + 1;
//   let content = `
//   <div id="czynn-ekspl${id}" class="form-group czynn-ekspl">
//     <div id="nazwa-czynnosci${id}" class="list-group-item">${listaCzynnosciEksploatacyjnych[id]}</div>
//     <span class=" mr-2 ml-2">Data: </span>
//     <input id="data-czynnosci${id}" type="text" >
//     <span class=" mr-2 ml-2">Cena: </span><input id="cena-czynnosci${id}" type="number" min=0>
//     <span class=" mr-2 ml-2">Ilość: </span><input id="ilosc-czynnosci${id}" type="number" min=0>
//   </div>
// `;
let content = `
<div class="col-lg-12 m-2">
  <div class="input-group">
    <span class="input-group-addon input-eksploatacyjne">
      <input id='check-czynnosci-radio${id}' class="czynnosci-eksploatacyjne-checkbox" type="checkbox" aria-label="Checkbox for following text input">
      <span id='nazwa-czynnosci${id}' class='nazwa-eksploatacyjne m-1'>${listaCzynnosciEksploatacyjnych[id].replace(/_/g,' ')}</span>
      <input id='cena-czynnosci${id}' class="cena-czynnosci-eksploatacyjnej" type="number" min=0 class="form-control" aria-label="Text input with checkbox" placeholder='Cena w złotych'>
    </span>
  </div>
</div>
`;
document.getElementById("lista-czynnosci-eksploatacyjnych-body").innerHTML +=content;



};
const renderWypozyczenia = () => {
  document.getElementsByClassName("container-fluid")[0].classList.remove("loader");



  //document.getElementById("sel1").disabled = true;
  DOMtable.innerHTML = "";
  let content = "";

  for (let i = 0; i < Wypozyczenia.length; i++) {
    let td = document.createElement("TH");
    td.setAttribute("id", `td${Wypozyczenia[i].id}`);
    let mode = "";
    let btn = document.createElement("BUTTON");
    let dodawacprzycisk = false;
    if (Wypozyczenia[i].status_wypozyczenia === "ZAREZERWOWANE") {
      btn.innerHTML = "WYPOŻYCZ";
      dodawacprzycisk = true;
      btn.setAttribute("data-target", `#wypozycz-pojazd`);
      mode = "wypozycz";

    } else if (Wypozyczenia[i].status_wypozyczenia === "WYPOŻYCZONE") {
      btn.innerHTML = "ZAKOŃCZ";
      dodawacprzycisk = true;
      btn.setAttribute("data-target", `#zakoncz-wypozyczenie`);
      mode = "zakoncz";

    }
    btn.setAttribute("id", `btn${Wypozyczenia[i].id}`);
    btn.setAttribute("class", `wypozyczenia-button btn btn-primary btn-sm`);
    btn.setAttribute("data-toggle", `modal`);

    btn.addEventListener("click", (e) => {
      podajIdPojazdu = Wypozyczenia[i].id_pojazdu;      
      podajPlanowanaDateRozpoczecia = Wypozyczenia[i].planowana_data_rozpoczecia;
      podajIdPracownika = Wypozyczenia[i].id_pracownika;
      if (e.toElement.innerHTML === "WYPOŻYCZ") {
        let getId = e.target.parentElement.parentElement.childNodes[0].innerHTML;
        podajIdWypozyczenia = getId;
        let content = `
<div class="input-group">
<span class="input-group-addon" id="basic-addon1">Przebieg rozpoczecia</span>
<input id="przebieg-rozpoczecia" type="number" class="form-control" min=0 placeholder="" aria-describedby="basic-addon1">
</div>
<br>
<span>Data rozpoczecia</span>
<input id="start-date" type="text" >
<br>

`;
        document.getElementById("wypozycz-pojazd-body").innerHTML = content;
        $(function () {
          $('#start-date').datetimepicker({
            lang: 'pl',
            dayOfWeekStart: 1,
            minDate: podajPlanowanaDateRozpoczecia
          });
        });


        //updateDoBazy("")
        e.toElement.innerHTML = "ZAKOŃCZ"

      } else if (e.toElement.innerHTML === "ZAKOŃCZ") {
       
        let getId = e.target.parentElement.parentElement.childNodes[0].innerHTML;
        podajIdWypozyczenia = getId;
        let content = `
          <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">Przebieg zakończenia</span>
          <input id="przebieg-zakonczenia" type="number" min=0 class="form-control" placeholder="" aria-describedby="basic-addon1">
          </div>
          <br>
          <span>Data zakończenia</span>
          <input id="end-date" type="text" >
          <br>
          <br>
          <div id="lista-czynnosci-eksploatacyjnych-body" class="">
          </div>
          `;
          if(mode ==="wypozycz"){
        document.getElementById("wypozycz-pojazd-body").innerHTML = content;
          } else {
            document.getElementById("zakoncz-wypozyczenie-body").innerHTML = content;
          }

        $(function () {
          $('#end-date').datetimepicker({
            lang: 'pl',
            dayOfWeekStart: 1,
            minDate: podajPlanowanaDateRozpoczecia
          });
        });


        //updateDoBazy("")
        e.toElement.innerHTML = "ZAKOŃCZ";
        for(let i = 0;i < listaCzynnosciEksploatacyjnych.length; i+=1){
          dodajPoleWyboruCzynnosciEksploatacyjnej(i);
        }
      }
      else {
        // STRZAŁ DO BAZY Z DANYMI
        let elements = e.target.parentElement.parentElement.childNodes;
        // let length = e.target.parentElement.parentElement.childNodes.length;
        // let data = [];
        let id = elements[0].innerHTML;

        // Pracownicy[id-1].rola = elements[1].getElementsByTagName("input")[0].value;
        // Pracownicy[id-1].imie = elements[2].getElementsByTagName("input")[0].value;
        // Pracownicy[id-1].nazwisko = elements[3].getElementsByTagName("input")[0].value;
        //Wypozyczenia[id - 1].status_zatrudnienia = elements[6].getElementsByTagName("select")[0].value;


        // let ele = elements[4].getElementsByTagName("select");
        // var strUser = e.options[e.selectedIndex].value;



        // for (let i = 1; i < length -2; i++) {
        //   // USUWAM INPUTA
        //   elements[i].getElementsByTagName("input")[0].remove(1);
        // }

        // wywołuje render
        renderWypozyczenia();


        e.toElement.innerHTML = "POTWIERDŹ";
      }


    });
    let wypozyczenie = document.createElement("TR");
    wypozyczenie.setAttribute("id", `wypozyczenie${Wypozyczenia[i].id}`);

    let text = "";

    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].id}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);



    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].planowana_data_rozpoczecia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].planowana_data_zakonczenia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].status_wypozyczenia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);
    // EXTRA TYLKO DLA ZAKONCZONCYCH
    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].faktyczna_data_rozpoczecia===null?0:Wypozyczenia[i].faktyczna_data_rozpoczecia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].faktyczna_data_zakonczenia===null?0:Wypozyczenia[i].faktyczna_data_zakonczenia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].przebieg_rozpoczecia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Wypozyczenia[i].przebieg_zakonczenia}`);
    td.appendChild(text);
    wypozyczenie.appendChild(td);

    // <button id="dodaj-wypozyczenie-btn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#dodaj-wypozyczenie">

    td = document.createElement("TD");
    td.setAttribute("id", `add-info-btn`);
    let showCarInfoBtn = document.createElement("BUTTON");
    showCarInfoBtn.innerHTML = "Pokaż informacje";
    showCarInfoBtn.setAttribute("id", `dodaj-wypozyczenie-btn`);
    showCarInfoBtn.setAttribute("data-target", `#carInfo`);
    showCarInfoBtn.setAttribute("data-toggle", `modal`);
    showCarInfoBtn.addEventListener("click", (e) => {
      const func = (pojazd) => {
        let text = `
        <ul class="list-group">
          <li class="list-group-item"><b>Marka:</b> ${pojazd.marka.nazwa}</li>
          <li class="list-group-item"><b>Model:</b> ${pojazd.model.nazwa}</li>
          <li class="list-group-item"><b>Rok produkcji:</b> ${pojazd.model.rok_prod}</li>
          <li class="list-group-item"><b>Nadwozie:</b> ${pojazd.model.nadwozie}</li>
          <li class="list-group-item"><b>Przebieg:</b> ${pojazd.przebieg}</li>
        </ul>
        `;
        document.getElementById('carInfo-body').innerHTML = text;

      };
      zapytanie(urlPojazdy, func, Wypozyczenia[i].id_pojazdu);

    });
    td.appendChild(showCarInfoBtn);
    wypozyczenie.appendChild(td);
    if (dodawacprzycisk) {
      td = document.createElement("TD");
      td.setAttribute("id", `edit-btn`);
      td.appendChild(btn);

      if(Wypozyczenia[i].status_wypozyczenia==='ZAREZERWOWANE'){
        let usun = document.createElement("BUTTON");
        usun.setAttribute("class", ` btn btn-danger w-100 btn-sm mt-1`);
        usun.innerHTML = "USUŃ";
        usun.addEventListener("click", (e) => {
         
            let getId = e.target.parentElement.parentElement.childNodes[0].innerHTML;
            usunZBazy(urlWypozyczenia, getId);
            setTimeout(() => {
              window.location.reload(true);
            }, 1000);
          });
          td.appendChild(usun);

      }
     
      wypozyczenie.appendChild(td);
    }

    DOMtable.appendChild(wypozyczenie); // PUT ALL OF THEM

  }
blokada();
};


const zbierzDaneDoWypozyczeniaPojazdu = () => {
  const faktyczna_data_rozpoczecia = document.getElementById("start-date").value;
  const przebieg_rozpoczecia = parseInt(document.getElementById("przebieg-rozpoczecia").value);
  const status_wypozyczenia = "WYPOŻYCZONE";
if(!faktyczna_data_rozpoczecia){ alert("Wpisz datę rozpoczęcia"); return false;}
if(!przebieg_rozpoczecia){ alert("Wpisz przebieg rozpoczęcia"); return false;}

  const obj = { faktyczna_data_rozpoczecia, przebieg_rozpoczecia, status_wypozyczenia };
  return obj;
};
const zbierzDaneDoZakonczeniaWypozyczeniaPojazdu = () => {
  const faktyczna_data_zakonczenia = document.getElementById("end-date").value;
  const przebieg_zakonczenia = parseInt(document.getElementById("przebieg-zakonczenia").value);
  const status_wypozyczenia = "ZAKOŃCZONE";
  if(!faktyczna_data_zakonczenia){ alert("Wpisz datę zakończenia"); return false;}
  if(!przebieg_zakonczenia){ alert("Wpisz przebieg zakończenia"); return false;}

  const obj = { faktyczna_data_zakonczenia, przebieg_zakonczenia, status_wypozyczenia };
  return obj;
};
const zbierzDaneCzynnosciEksploatacyjnych = () => {
  let nazwa, cena, ilosc, id_pojazdu;
  let obj = {};
  const obj2 = [];
  let data = document.getElementById("end-date").value;
  data = data.substring(0,10);

  for(let i = 0; i < ile; i += 1){
    nazwa = listaCzynnosciEksploatacyjnych[i];
    cena = parseInt(document.getElementById(`cena-czynnosci${i}`).value);
    let check = document.getElementById('check-czynnosci-radio'+ i).checked;
    if(!cena || !check){continue;}
   
    const id_pojazdu = podajIdPojazdu;
    const id_pracownika = podajIdPracownika;
    obj = { nazwa, data, cena, id_pojazdu, id_pracownika };
    postDoBazy(urlCzynnosciEksploatacyjnych,obj);
    setTimeout(() => {
      
    }, 200);
  }
  return obj2;
};

const makeDisableEnable = () => {

};