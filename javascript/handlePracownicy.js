let Pracownicy = [];
const DOMtable = document.getElementById("pracownicy");
document.addEventListener("DOMContentLoaded", (event) => {
  document.getElementsByClassName("container-fluid")[0].classList.add("loader");

  zapytanie(urlStatusyZatrudnienia, wczytajStatusyZatrudnienia);
  zapytanie(urlPracownicy, wczytajPracownikow);

});


const renderPracownicy = () => {
  document.getElementsByClassName("container-fluid")[0].classList.remove("loader");



  //document.getElementById("sel1").disabled = true;
  DOMtable.innerHTML = "";
  let content = "";

  for (let i = 0; i < Pracownicy.length; i++) {
    let td = document.createElement("TH");
    td.setAttribute("id", `td${Pracownicy[i].id}`);

    let btn = document.createElement("BUTTON");
    btn.innerHTML = "EDYTUJ";
    btn.setAttribute("id", `btn${Pracownicy[i].id}`);
    btn.addEventListener("click", (e) => {
      if (e.toElement.innerHTML === "EDYTUJ") {

        document.getElementById(`sel${i}`).disabled = false;
        const editBtn = document.getElementById("edit-btn");
        // let values = e.target.parentElement.parentElement.childNodes[1].textContent;
        // let elements = e.target.parentElement.parentElement.childNodes;
        // let length = e.target.parentElement.parentElement.childNodes.length;
        // for (let i = 1; i < length - 2; i++) {
        //   let inp = document.createElement("INPUT");
        //   inp.setAttribute("class", `cover`);
        //   inp.value = elements[i].textContent;
        //   elements[i].appendChild(inp);
        // } 
        e.toElement.innerHTML = "ZAPISZ"

      } else {
        // STRZAŁ DO BAZY Z DANYMI
        let elements = e.target.parentElement.parentElement.childNodes;
        // let length = e.target.parentElement.parentElement.childNodes.length;
        // let data = [];
        let id = elements[0].innerHTML;
        const status_zatrudnienia = elements[6].getElementsByTagName("select")[0].value;
        const obj = { id, status_zatrudnienia };
        updateDoBazy(obj, urlPracownicy, id);

        // Pracownicy[id-1].rola = elements[1].getElementsByTagName("input")[0].value;
        // Pracownicy[id-1].imie = elements[2].getElementsByTagName("input")[0].value;
        // Pracownicy[id-1].nazwisko = elements[3].getElementsByTagName("input")[0].value;
        Pracownicy[id - 1].status_zatrudnienia = elements[6].getElementsByTagName("select")[0].value;


        // let ele = elements[4].getElementsByTagName("select");
        // var strUser = e.options[e.selectedIndex].value;



        // for (let i = 1; i < length -2; i++) {
        //   // USUWAM INPUTA
        //   elements[i].getElementsByTagName("input")[0].remove(1);
        // }

        // wywołuje render
        renderPracownicy();


        e.toElement.innerHTML = "EDYTUJ";
      }


    });
    let pracownik = document.createElement("TR");
    pracownik.setAttribute("id", `pracownicy${Pracownicy[i].id}`);

    let text = "";

    td = document.createElement("TD");
    text = document.createTextNode(`${Pracownicy[i].id}`);
    td.appendChild(text);
    pracownik.appendChild(td);


    td = document.createElement("TD");
    text = document.createTextNode(`${Pracownicy[i].rola}`);
    td.appendChild(text);
    pracownik.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pracownicy[i].pesel}`);
    td.appendChild(text);
    pracownik.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pracownicy[i].imie}`);
    td.appendChild(text);
    pracownik.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pracownicy[i].nazwisko}`);
    td.appendChild(text);
    pracownik.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pracownicy[i].data_urodzenia}`);
    td.appendChild(text);
    pracownik.appendChild(td);


    td = document.createElement("TD");
    let status = statusy_zatrudnienia.map((item) => {
      return `<option>${item}</option>`
    });

    let idd = "sel" + i;
    text = `
    <div class="form-group">
    <select onchange='pokazAlertPracownikow(this, "${Pracownicy[i].rola}")' class="form-control" id=${idd}>
    ${statusy_zatrudnienia.map((item) => `<option value='${item}'>${item.replace("_", " ")}</option>`)}
    </select>
    </div>`;
    td.innerHTML = text;
    pracownik.appendChild(td);

    td = document.createElement("TD");
    td.setAttribute("id", `edit-btn`);
    td.appendChild(btn);
    pracownik.appendChild(td);

    DOMtable.appendChild(pracownik);
    let temp = Pracownicy[i].status_zatrudnienia;
    document.getElementById(idd).value = temp;
    document.getElementById(idd).disabled = true;
  }

  blokada();



};
const pokazAlertPracownikow = (e, rola) => {
  const value = e.options[e.selectedIndex].value;
  if (value === statusy_zatrudnienia[1] || value === statusy_zatrudnienia[2]) {
    if (rola === 'OPIEKUN') {
      alert("Uwaga.Wybierając ten status wszystkie wypożyczenia danego pracownika zostaną usunięte oraz jego OPIEKI zostaną usunięte");
    } else {
      alert("Uwaga.Wybierając ten status wszystkie wypożyczenia danego pracownika zostaną usunięte.")
    }
  }

}