
const selectAsActive = (e) => {
  var current = document.getElementsByClassName("aktualny-wybor");
  if (current.length > 0) {
    current[0].className = current[0].className.replace(" aktualny-wybor", "");
  }
  e.target.parentElement.className += " aktualny-wybor";
};
let wyborPojazduDoRezerwacji = {};

document.addEventListener("DOMContentLoaded", (event) => {

  drawSerwisButtons();
  document.getElementById("oddaj-do-serwisu-btn").addEventListener("click", oddajDoSerwisu);
  document.getElementById("oddaj-do-serwisu-do-bazy-btn").addEventListener("click", (e) => {
    updateDoBazy({
      "status": "W_NAPRAWIE"
      //, 
      //'data': new Date().toLocaleString('en-us',{hour12:false}).replace(',','')
    }, urlPojazdy, wyborPojazduDoRezerwacji.id);
    setTimeout(() => {
      window.location.reload(true);
    }, 700);
  });
  document.getElementById("odbierz-z-serwisu-btn").addEventListener("click", odbierzZSerwisu);
  document.getElementById("odbierz-z-serwisu-do-bazy-btn").addEventListener("click", (e) => {

    if (zbierzDaneCzynnosciSerwisowych()) {
      setTimeout(() => {
        window.location.reload(true);
      }, 700);

    }
  });
  zapytanie(urlListaCzynnosciSerwisowych, wczytajListeCzynnosciSerwisowych);
  blokada();
});

const oddajDoSerwisu = () => {
  console.log(urlPojazdySprawne, ' <= GET po samochody do oddania do serwisu');
  fetch(urlPojazdySprawne, { method: 'get' })
    .then((resp) => resp.json()) // Transform the data into json
    .then(function (data) {
      pokazPojazdy(data);
    });
  setTimeout(() => {

  }, 200);
  let content = `
  <div class="card mb-3">
  <div class="card-header">
  <p class="bg-danger p-1"> 
  Wszystkie rezerwacje pojazdu oddanego do serwisu zostaną automatycznie anulowane.
  </p>
    <i class="fa fa-table"></i> Lista dostepnych pojazdów
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Model</th>
            <th>Marka</th>
            <th>Nr rej</th>
            <th>Przebieg</th>
            <th>Nadwozie</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Marka</th>
            <th>Model</th>
            <th>Nr rej</th>
            <th>Przebieg</th>
            <th>Nadwozie</th>
          </tr>
        </tfoot>
        <tbody id="lista-pojazdow">

        </tbody>
      </table>
    </div>
  </div>
</div>
              `;

  document.getElementById("oddaj-do-serwisu-body").innerHTML = content;
};
const odbierzZSerwisu = () => {
  console.log(urlPojazdySerwisowane, 'GET po liste samochodow w naprawie')
  fetch(urlPojazdySerwisowane, { method: 'get' })
    .then((resp) => resp.json()) // Transform the data into json
    .then(function (data) {
      pokazPojazdyDoOdbioru(data);
    });
  setTimeout(() => {

  }, 200);

  dodajListeCzynnosciSerwisowych();

  let content = `fdsa`;
  // document.getElementById("odbierz-z-serwisu-body").innerHTML = content;
};

const drawSerwisButtons = () => {
  let content = "";
  // SERWIS
  content += `
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="oddaj-do-serwisu" tabindex="-1" role="dialog" aria-labelledby="oddaj-do-serwisu" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="oddaj-do-serwisu">Oddaj pojazd do serwisu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="oddaj-do-serwisu-body" class="modal-body">
      <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Lista dostepnych pojazdów
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Model</th>
                <th>Marka</th>
                <th>Nr rej</th>
                <th>Przebieg</th>
                <th>Nadwozie</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>ID</th>
                <th>Marka</th>
                <th>Model</th>
                <th>Nr rej</th>
                <th>Przebieg</th>
                <th>Nadwozie</th>
              </tr>
            </tfoot>
            <tbody id="lista-pojazdow">
    
            </tbody>
          </table>
        </div>
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
        <button id="oddaj-do-serwisu-do-bazy-btn" type="button" class="btn btn-primary">Oddaj</button>
      </div>
    </div>
  </div>
</div>`;

  content += `
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="odbierz-z-serwisu" tabindex="-1" role="dialog" aria-labelledby="odbierz-z-serwisu" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content text-center">
      <div class="modal-header">
        <h5 class="modal-title" id="odbierz-z-serwisu">Odbierz pojazd z serwisu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="odbierz-z-serwisu-main" class="modal-body">
      <div class="input-group">
        <span class="input-group-addon mr-1" id="basic-addon1">Data odbioru</span>
        <input id="data-odbioru-z-serwisu" type="text" >
      </div>

      <div class="card mb-3">
      <div class="card-header">
        <i class="fa fa-table"></i> Lista dostepnych pojazdów
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Model</th>
                <th>Marka</th>
                <th>Nr rej</th>
                <th>Przebieg</th>
                <th>Nadwozie</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>ID</th>
                <th>Marka</th>
                <th>Model</th>
                <th>Nr rej</th>
                <th>Przebieg</th>
                <th>Nadwozie</th>
              </tr>
            </tfoot>
            <tbody id="lista-pojazdow-do-odbioru">
    
            </tbody>
          </table>
        </div>
      </div>
    </div>

      <div id="odbierz-z-serwisu-body"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
        <button id="odbierz-z-serwisu-do-bazy-btn"type="button" class="btn btn-primary">Odbierz</button>
      </div>
    </div>
  </div>
</div>`;

  document.getElementById("add-buttons").innerHTML += content;
  $(function () {
    $(`#data-odbioru-z-serwisu`).datetimepicker({
      lang: 'pl',
      dayOfWeekStart: 1,
      timepicker: false,
      format: 'Y/m/d'
    });
  });

};


// wopw
const pokazPojazdy = (data) => {

  for (let i = 0; i < data.length; i += 1) {
    let container = document.createElement("TR");
    container.addEventListener("click", (e) => {
      selectAsActive(e);
      wyborPojazduDoRezerwacji = data[i];
    });

    container.setAttribute("id", `container${data[i].id}`);
    // inicjuj perwszy element jako wybor
    if (i === 0) {
      container.setAttribute("class", `cursor-pointer aktualny-wybor`);
      wyborPojazduDoRezerwacji = data[i];
    } else {
      container.setAttribute("class", `cursor-pointer`);
    }
    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].id}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].model.nazwa}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].marka.nazwa}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].nr_rej}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].przebieg}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].model.nadwozie}`);
    td.appendChild(text);
    container.appendChild(td);


    document.getElementById("lista-pojazdow").appendChild(container);
  };

};
const pokazPojazdyDoOdbioru = (data) => {
  document.getElementById("lista-pojazdow-do-odbioru").innerHTML = '';
  for (let i = 0; i < data.length; i += 1) {
    let container = document.createElement("TR");
    container.addEventListener("click", (e) => {
      selectAsActive(e);
      wyborPojazduDoRezerwacji = data[i];
    });

    container.setAttribute("id", `container${data[i].id}`);
    // inicjuj perwszy element jako wybor
    if (i === 0) {
      container.setAttribute("class", `cursor-pointer aktualny-wybor`);
      wyborPojazduDoRezerwacji = data[i];
    } else {
      container.setAttribute("class", `cursor-pointer`);
    }
    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].id}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].model.nazwa}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].marka.nazwa}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].nr_rej}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].przebieg}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].model.nadwozie}`);
    td.appendChild(text);
    container.appendChild(td);


    document.getElementById("lista-pojazdow-do-odbioru").appendChild(container);
  };

};

const dodajListeCzynnosciSerwisowych = () => {
  let content = '';
  for (let id = 0; id < listaCzynnosciSerwisowych.length; id++) {
    content += `
   <div class="col-lg-6 m-2">
   <div class="input-group">
     <span class="input-group-addon">
       <input id='radio-czynnosci-serwisowej${id}' type="checkbox" aria-label="Checkbox for following text input">
       <span id='nazwa-czynnosci-serwisowej${id}' class='m-1'>${listaCzynnosciSerwisowych[id].replace(/_/g,' ')}</span>
       <input id='cena-czynnosci-serwisowej${id}' type="number" min=0 class="form-control" aria-label="Text input with checkbox" placeholder='Cena w złotych'>
       </span>
   </div>
 </div>
    `;
  }
  document.getElementById('odbierz-z-serwisu-body').innerHTML = content;
};

const zbierzDaneCzynnosciSerwisowych = () => {
let nie = 0;
  const data = document.getElementById('data-odbioru-z-serwisu').value;
  if (!data) { alert("Nie podales daty"); return false; }

  for (let i = 0; i < listaCzynnosciSerwisowych.length; i++) {
    const radio = document.getElementById(`radio-czynnosci-serwisowej${i}`).checked;
    if (!radio) { nie+=1;continue; }
    const nazwa = listaCzynnosciSerwisowych[i];
    const cena = parseInt(document.getElementById(`cena-czynnosci-serwisowej${i}`).value);
    const id_pracownika = parseInt(localStorage.getItem('id_pracownika'));
    const obj = { nazwa, cena, data, 'id_pojazdu': wyborPojazduDoRezerwacji.id, id_pracownika }
    postDoBazy(urlCzynnosciSerwisowych, obj);
  }
  if(nie===listaCzynnosciSerwisowych.length){
    alert("Nie wybrales czynnosci serwisowej"); return false;
  }
  setTimeout(() => {
    updateDoBazy({ "status": "SPRAWNY" }, urlPojazdy, wyborPojazduDoRezerwacji.id);
  }, 150);
  return true;
};