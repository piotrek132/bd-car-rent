
const blokada = () => {
  if (localStorage.getItem('rola') === "KIEROWCA") {
    document.getElementById("zakladka-dane").style.display = "none";
    document.getElementById("zakladka-pracownicy").style.display = "none";
    document.getElementById("zakladka-pojazdy").style.display = "none";
    document.getElementById("zakladka-serwis").style.display = "none";
    let length = document.getElementsByClassName('wypozyczenia-button').length;
    if (length) {
      for (let i = 0; i < length; i += 1) {

        document.getElementsByClassName('wypozyczenia-button')[i].style.display = "none";
      }
    }

    if (window.location.href.indexOf("index.html") !== -1) {
      document.getElementById("dodaj-pojazd-btn").style.display = "none";
      document.getElementById("dodaj-pracownika-btn").style.display = "none";
    }


  } else if (localStorage.getItem('rola') === "OPIEKUN") {
    document.getElementById("zakladka-pracownicy").style.display = "none";
    document.getElementById("zakladka-dane").style.display = "none";
    if (window.location.href.indexOf("index.html") !== -1) {

    document.getElementById("dodaj-pojazd-btn").style.display = "none";
    document.getElementById("dodaj-wypozyczenie-btn").style.display = "none";
    document.getElementById("dodaj-pracownika-btn").style.display = "none";
    }


  } else if (localStorage.getItem('rola') === "MENADŻER") {
    document.getElementById("zakladka-wypozyczenia").style.display = "none";
    document.getElementById("zakladka-serwis").style.display = "none";
    if (window.location.href.indexOf("index.html") !== -1) {
      document.getElementById("dodaj-wypozyczenie-btn").style.display = "none";
    }
  } else {
    console.log('brak usera')
  }
}

