// Chart.js scripts
// -- Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';
// -- Area Chart Example
let pojazdyId = [];
let idPojazdu = 1;
let miesiac = 'Styczeń';
let wszystkieMiesieczneWydatki = [];
let wszystkieWydatki = [];
let wydatkiMiesieczneNaPojazd = [];
const miesiace = ["STYCZEŃ", "LUTY", "MARZEC", "KWIECIEŃ", "MAJ", "CZERWIEC", "LIPIEC", "SIERPIEŃ", "WRZESIEŃ", "PAŹDZIERNIK", "LISTOPAD", "GRUDZIEŃ"];
zapytanie(urlListaCzynnosciEksploatacyjnych, wczytajListeCzynnosciEksploatacyjnych);
zapytanie(urlListaCzynnosciSerwisowych, wczytajListeCzynnosciSerwisowych);

console.log(urlWydatkiKierowcy, ' <= strzelam GET po wydatki wszystkich kierowcow');
fetch(urlWydatkiKierowcy, { method: 'get' })
  .then((resp) => resp.json()) // Transform the data into json
  .then(function (data) {
    
    Kierowca = data;
    renderPracownicyChart();

  })
  
  const pokazMiesieczneWydatki = () => {
    console.log(urlWszystkieMiesieczneWydatki, ' <= strzelam GET po wszytkie miesieczne wydatki');
  fetch(urlWszystkieMiesieczneWydatki, { method: 'get' })
    .then((resp) => resp.json()) // Transform the data into json
    .then(function (data) {
      wszystkieWydatki = [];
      const rokWydatku = parseInt(document.getElementById('wszystkie-wydatki-rok').value);
      wszystkieMiesieczneWydatki = [];
      // wszystkieMiesieczneWydatki = data.dane; // pierwsze z brzegu czyli 2017 rok
      for(let i =0; i < data.length; i +=1){
        if(data[i].rok===rokWydatku){
          wszystkieWydatki.push(data[i]);
          wszystkieMiesieczneWydatki.push(data[i].cena);
        }
      }
      monthlyChart();
    })
};
document.addEventListener("DOMContentLoaded", (event) => {

  document.getElementsByClassName("container-fluid")[0].classList.add("loader");

  setTimeout(() => {
    pokazMiesieczneWydatki();


    // wczytajIdPojazdow 
    const nazwaTabeli = `
    <button id="print-wydatki-wszystkie" class="btn-info">DRUKUJ</button><br>
    Wszystkie miesięczne wydatki w roku  
    <select onchange="pokazMiesieczneWydatki()" id="wszystkie-wydatki-rok" class="form-control form-control-sm">
    <option>2018</option>
    <option>2017</option>
    </select>
    `;
    document.getElementById('nazwa-tabeli-slupki').innerHTML = nazwaTabeli;

    fetch(urlPojazdy, { method: 'get' })
      .then((resp) => resp.json()) //strzal po id pojazdow
      .then((data) => {
        Pojazdy = data;
        data.forEach(element => {
          pojazdyId.push(element.id);
        });
        wykresKolowy('fasd');
        //edytujWykresKolowy(dataKolowy,'Id pojazdu 1', listaCzynnosciEksploatacyjnych);

        document.getElementById("id_pojazdu_label").addEventListener("change", zmienWykresKolowy);
        document.getElementById("data-chart-choice").addEventListener("change", zmienWykresKolowy);
        document.getElementById("rok-wykres-kolowy").addEventListener("change", (e) => {zmienWykresKolowy(e,true)});

//  ==========================================---------------
//  ==========================================---------------
//  ==========================================---------------
//  ==========================================---------------
//dopisac id do jakiego buttona z drukarka
        document.getElementById("print-wydatki-kierowcow").addEventListener("click", () => {
          printJS({printable: Kierowca, type: 'json', properties: ['id_pracownika', 'wydatki'], header:'wydatki kierowców' });
        });
        document.getElementById("print-wydatki-wszystkie").addEventListener("click", () => {
          printJS({printable: wszystkieWydatki, type: 'json', properties: ['rok', 'miesiac', 'cena'], header:'Wszystkie wydatki na miesiąc' });
        });

        document.getElementById("print-wydatki-pojazdu").addEventListener("click", () => {
          printJS({printable: wydatkiMiesieczneNaPojazd.wydatek, type: 'json', properties: ['nazwa', 'cena'], header:'Id pojazdu: '+jakieIdPojazdu });
          });

      });
    // na start wypelnij syczniem
    let dataKol = [];
    console.log(urlWydatkiMiesieczne + "?rok=2017&miesiac=01" + "&id_pojazdu=" + idPojazdu, '<= GET poo wydatki miesieczne na podstawie miesiaca i id pojazdu ')
    fetch(urlWydatkiMiesieczne + "?rok=2017&miesiac=01"  + "&id_pojazdu=" + idPojazdu, { method: 'get' })
      .then((resp) => resp.json()) //strzal po id pojazdow
      .then((data) => {
        if (data.wydatek.length) {
          jakieIdPojazdu = data.id_pojazdu;
          wydatkiMiesieczneNaPojazd = data;
          data.wydatek.forEach(element => {
            dataKol.push(parseInt(element.cena));
          });
          edytujWykresKolowy(dataKol, 'Id pojazdu 1', listaCzynnosciEksploatacyjnych);
        }
        else{
          document.getElementById("information-pieChart").innerHTML = "BRAK DANYCH";
          return;
        }
        setTimeout(() => {
          document.getElementById("id_pojazdu_label").addEventListener("change", zmienWykresKolowy);
          document.getElementById("data-chart-choice").addEventListener("change", zmienWykresKolowy);

        }, 200);
      });


  }, 500);
});

const monthlyChart = (nazwaSlupka = "cena") => {

  // -- Bar Chart Example
  var ctx = document.getElementById("myBarChart");
if(myLineChart){
  myLineChart.destroy();
}
  myLineChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
      datasets: [{
        label: nazwaSlupka,
        backgroundColor: "rgba(2,117,216,1)",
        borderColor: "rgba(2,117,216,1)",
        data: wszystkieMiesieczneWydatki,
      }],
    },
    options: {
      scales: {
        xAxes: [{
          time: {
            unit: 'miesiąc'
          },
          gridLines: {
            display: false
          },
          ticks: {
            maxTicksLimit: 6
          }
        }],
        yAxes: [{
          ticks: {
            min: 0,
            max: 700000,
            maxTicksLimit: 5
          },
          gridLines: {
            display: true
          }
        }],
      },
      legend: {
        display: false
      }
    }
  });
};
const zmienWykresKolowy = (e,extraYear) => {
  let rok = document.getElementById('rok-wykres-kolowy').value;
if(extraYear){
rok = e.target.value;
}
  else if (e.target.value.length<=2 ) { //aktualizuj id_pojazdu
    idPojazdu = e.target.value;
  } else {
    miesiac = e.target.value.toLowerCase();

  }
  let dataKol = [];
  if(miesiac==='2017' || miesiac==='2018'){
    console.log(urlWydatkiRoczne + "?rok=" + miesiac.toString() + "&id_pojazdu=" + idPojazdu, ' <- tak wygląda zapytanie o wydatki roczne');
    fetch(urlWydatkiRoczne + "?rok=" + miesiac.toString() + "&id_pojazdu=" + idPojazdu, { method: 'get' })
    .then((resp) => resp.json()) //strzal po id pojazdow
    .then((data) => {
      if (data.wydatek.length) {
        document.getElementById("information-pieChart").innerHTML = "";
        document.getElementById('print-wydatki-pojazdu').style.visibility = 'visible';
        jakieIdPojazdu = data.id_pojazdu;
        wydatkiMiesieczneNaPojazd = data;

        data.wydatek.forEach(element => {
          dataKol.push(parseInt(element.cena));
        });
        edytujWykresKolowy(dataKol, 'Id pojazdu 1', listaCzynnosciEksploatacyjnych);
      } else {
        if(myPieChart){
          myPieChart.destroy();
        }
          document.getElementById("information-pieChart").innerHTML = "BRAK DANYCH";
          document.getElementById('print-wydatki-pojazdu').style.visibility = 'hidden';

          return;
      }
      document.getElementById("id_pojazdu_label").addEventListener("change", zmienWykresKolowy);
      document.getElementById("data-chart-choice").addEventListener("change", zmienWykresKolowy);
    });
    return;
  }

  console.log(urlWydatkiMiesieczne + "?rok="+rok+"&miesiac=" +(miesiace.indexOf(miesiac.toUpperCase())+1) + "&id_pojazdu=" + idPojazdu, ' <- tak wygląda zapytanie o wydatki miesieczne');

  fetch(urlWydatkiMiesieczne + "?rok="+rok+"&"+ "miesiac=" + (miesiace.indexOf(miesiac.toUpperCase())+1) + "&id_pojazdu=" + idPojazdu, { method: 'get' })
    .then((resp) => resp.json()) //strzal po id pojazdow
    .then((data) => {
      if (data.wydatek.length) {
        document.getElementById("information-pieChart").innerHTML = "";
        
        document.getElementById('print-wydatki-pojazdu').style.visibility = 'visible';

        wydatkiMiesieczneNaPojazd = data;
        jakieIdPojazdu = data.id_pojazdu;
        data.wydatek.forEach(element => {
          dataKol.push(parseInt(element.cena));
        });
        edytujWykresKolowy(dataKol, 'Id pojazdu 1', listaCzynnosciEksploatacyjnych);
      }
      else{
        if(myPieChart){
        myPieChart.destroy();
        }
        document.getElementById("information-pieChart").innerHTML = "BRAK DANYCH";
        document.getElementById('print-wydatki-pojazdu').style.visibility = 'hidden';
        return;
      }
      document.getElementById("id_pojazdu_label").addEventListener("change", zmienWykresKolowy);
      document.getElementById("data-chart-choice").addEventListener("change", zmienWykresKolowy);
    });


};
var myPieChart;
var myLineChart;
const edytujWykresKolowy = (dane, nazwaWykresu, nazwy) => {
nazwy = nazwy.concat(listaCzynnosciSerwisowych)
  nazwy = nazwy.map((item)=> item.replace(/_/g,' '));
  var ctx = document.getElementById("myPieChart");
  if (myPieChart) {
    myPieChart.destroy();

  }
  myPieChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: nazwy,
      datasets: [{
        data: dane,
        backgroundColor: [
          '#6610f2',
          '#e83e8c',
          '#fd7e14',
          '#ffc10',
          '#28a745',
          '#868e96',
          '#20c997',
          '#6f42c1',
          '#868e96',
          '#343a40',
          '#007bff',
          '#dc3545',
          '#28a745',
          '#17a2b8',
          '#17a2b8',
          '#dc3545',
          '#f8f9fa',
          '#ffc107',
          '#343a40'],
      }],
    },
  });
};
const wykresKolowy = (nazwaWykresu) => {


    // -- Pie Chart Example

    document.getElementById("nazwa-wykres-kolowy").innerHTML = nazwaWykresu;
    document.getElementById("wykres-kolowy-container").innerHTML = `
  <i class="fa fa-pie-chart"></i> <span id="nazwa-wykres-kolowy"></span>
  <button id="print-wydatki-pojazdu" class="btn-info">DRUKUJ</button>
  <div class="form-group">
    <label for="id_pojazdu_label">Pojazd</label>
    <select class="form-control" id="id_pojazdu_label">
    ${pojazdyId.map((item,index) => `<option value=${item}>id: ${item} ${Pojazdy[index].marka.nazwa} ${Pojazdy[index].model.nazwa}</option>`)}
    </select>
  </div>
  <div class="form-group">
  <label for="rok-wykres-kolowy">Rok miesiąca</label>
  <select class="form-control" id="rok-wykres-kolowy">
  ${["2017", "2018"].map((item,index) => `<option value=${item}>${item}</option>`)}
  </select>
  </div>
  <div class="form-group">
  <label for="data-chart-choice">Data</label>
  <select class="form-control" id="data-chart-choice">
    <option value="2017">Cały rok 2017</option>
    <option value="2018">Cały rok 2018</option>
    <option selected>Styczeń</option>
    <option>Luty</option>
    <option>Marzec</option>
    <option>Kwiecień</option>
    <option>Maj</option>
    <option>Czerwiec</option>
    <option>Lipiec</option>
    <option>Sierpień</option>
    <option>Wrzesień</option>
    <option>Październik</option>
    <option>Listopad</option>
    <option>Grudzie</option>
  </select>
</div>`;

};


const SzerokiWykres = (nazwa = 'Wykres') => {
  var ctx = document.getElementById("myAreaChart");
  document.getElementById("szeroki-wykres-nazwa").innerHTML = nazwa;
  if (myLineChart) {
    myLineChart.destroy();
  }
  myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: ["Mar 1", "Mar 2", "Mar 3", "Mar 4", "Mar 5", "Mar 6", "Mar 7", "Mar 8", "Mar 9", "Mar 10", "Mar 11", "Mar 12", "Mar 13"],
      datasets: [{
        label: "Sessions",
        lineTension: 0.3,
        backgroundColor: "rgba(2,117,216,0.2)",
        borderColor: "rgba(2,117,216,1)",
        pointRadius: 5,
        pointBackgroundColor: "rgba(2,117,216,1)",
        pointBorderColor: "rgba(255,255,255,0.8)",
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(2,117,216,1)",
        pointHitRadius: 20,
        pointBorderWidth: 2,
        data: [10000, 30162, 26263, 18394, 18287, 28682, 31274, 33259, 25849, 24159, 32651, 31984, 38451],
      }],
    },
    options: {
      scales: {
        xAxes: [{
          time: {
            unit: 'date'
          },
          gridLines: {
            display: false
          },
          ticks: {
            maxTicksLimit: 7
          }
        }],
        yAxes: [{
          ticks: {
            min: 0,
            max: 40000,
            maxTicksLimit: 5
          },
          gridLines: {
            color: "rgba(0, 0, 0, .125)",
          }
        }],
      },
      legend: {
        display: false
      }
    }
  });
};


const DOMtable = document.getElementById('pracownicy-dane-firmy');

const renderPracownicyChart = () => {
  document.getElementsByClassName("container-fluid")[0].classList.remove("loader");

  DOMtable.innerHTML = "";
  for (let i = 0; i < Kierowca.length; i++) {
    let td = document.createElement("TH");
    let pracownik = document.createElement("TR");

    td = document.createElement("TD");
    text = document.createTextNode(`${Kierowca[i].id_pracownika}`);
    td.appendChild(text);
    pracownik.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Kierowca[i].wydatki}`);
    td.appendChild(text);
    pracownik.appendChild(td);

    DOMtable.appendChild(pracownik);
  }

  blokada();
};