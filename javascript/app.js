
let podajIdOpiekunaDoWypozyczenia = null;
let wyborPojazduDoRezerwacji = {};

document.addEventListener("DOMContentLoaded", (event) => {

  zapytanie(urlStatusyPojazdu, wczytajStatusyPojazdow);
  zapytanie(urlOpiekunow, wczytajOpiekunow);
  zapytanie(urlStatusyZatrudnienia, wczytajStatusyZatrudnienia);
  zapytanie(urlRolePracownikow, wczytajRolePracownikow);
  zapytanie(urlNadwozia, wczytajNadwozia);

  drawModal();
  document.getElementById("dodaj-pojazd-btn").addEventListener("click", dodajPojazd);
  document.getElementById("dodaj-wypozyczenie-btn").addEventListener("click", dodajWypozyczenie);
  document.getElementById("dodaj-pracownika-btn").addEventListener("click", dodajPracownika);

  document.getElementById("szukaj-pojazdow-do-rezerwacji-btn").addEventListener("click", szukajPojazdowDoRezerwacji);

  document.getElementById("dodaj-pojazd-do-bazy").addEventListener("click", (e) => {

    if (daneNowegoPojazdu()) {
      dodajPojazdDoBazy(daneNowegoPojazdu(), urlPojazdy);
      $('#dodaj-pojazd').modal('hide');
    }
  });
  document.getElementById("dodaj-pracownika-do-bazy").addEventListener("click", (e) => {
    if (daneNowegoPracownika()) {
      dodajPracownikaDoBazy(daneNowegoPracownika(), urlPracownicy);
      $('#dodaj-pracownika').modal('hide');
    }
  });
  document.getElementById("dodaj-wypozyczenie-do-bazy").addEventListener("click", (e) => {
    if(daneNowegoWypozyczenia()){
      postDoBazy(urlWypozyczenia, daneNowegoWypozyczenia());
      $('#dodaj-wypozyczenie').modal('hide');
      wyborPojazduDoRezerwacji = {};
    }
  });


  // dodanie datetimepickerow
  $(function () {
    $('#datetimepicker').datetimepicker({
      allowTimes: [
        '12:00', '13:00', '15:00',
        '17:00', '17:05', '17:20', '19:00', '20:00'
      ],
      lang: 'pl',
      dayOfWeekStart: 1,
    });
    // z zakresem

    // $('#datetimepicker').datetimepicker({
    //   format:'d.m.Y H:i',
    //   lang:'pl'
    // });

    $.datetimepicker.setLocale('pl');
  });

  blokada();

});

const daneNowegoWypozyczenia = () => {
  // zbierz dane nowego pojazdu
  const planowana_data_rozpoczecia = document.getElementById("data_with_range_start").value;
  const planowana_data_zakonczenia = document.getElementById("data_with_range_end").value;
  const faktyczna_data_rozpoczecia = "";
  const faktyczna_data_zakonczenia = "";
  const przebieg_rozpoczecia = 0;
  const przebieg_zakonczenia = 0;
  const id_pracownika = parseInt(localStorage.getItem("id_pracownika"));
  const id_pojazdu = parseInt(wyborPojazduDoRezerwacji.id);
  const status_wypozyczenia = "ZAREZERWOWANE";
if(wyborPojazduDoRezerwacji.id===undefined){alert("Pojazd nie został wybrany"); return false;}
if(!planowana_data_rozpoczecia){alert("Nieprawidłowa data rozpoczęcia"); return false;}
if(!planowana_data_zakonczenia){alert("Nieprawidłowa data zakończenia"); return false;}
if(planowana_data_rozpoczecia>= planowana_data_zakonczenia){alert("Nieprawidłowy zakres dat"); return false;}

  const obj = {
    planowana_data_rozpoczecia, planowana_data_zakonczenia, faktyczna_data_rozpoczecia, faktyczna_data_zakonczenia,
    przebieg_rozpoczecia, przebieg_zakonczenia, id_pracownika, id_pojazdu, status_wypozyczenia
  };
  return obj;
};
const daneNowegoPojazdu = () => {
  // zbierz dane nowego pojazdu

  const nazwa = document.getElementById("data-nazwa-modelu").value;
  const marka = document.getElementById("data-nazwa-marki").value;
  const rok_prod = parseInt(document.getElementById("data-rok-prod").value);
  const przebieg = parseInt(document.getElementById("data-przebieg").value);
  const status = document.getElementById("data-stan").value;
  const nr_rej = document.getElementById("data-nr-rej").value;
  const nadwozie = document.getElementById("data-typ-nadwozia").value;
  const ilosc_miejsc = parseInt(document.getElementById("data-ilosc-miejsc").value);
  const opiekun = document.getElementById("data-opiekunowie").value;
  const id_opiekuna = podajIdOpiekunaDoWypozyczenia;

  if (!marka) { alert("Nie podano nazwy marki"); return false; }
  if (!nazwa) { alert("Nie podano nazwy modelu"); return false; }
  if (!rok_prod) { alert("Nie podano roku produkcji"); return false; }
  if (!przebieg) { alert("Nie podano przebiegu"); return false; }
  if (!nr_rej) { alert("Nie podano numeru rejestracyjnego"); return false; }
  if (!ilosc_miejsc) { alert("Nie podano ilosci miejsc"); return false; }

  const obj = { opiekun, id_opiekuna, nr_rej, przebieg, status };
  obj.marka = { "nazwa": marka };
  obj.model = { nazwa, ilosc_miejsc, rok_prod, nadwozie };
  return obj;
};
const daneNowegoPracownika = () => {
  // zbierz dane nowego pracownika
  const pesel = document.getElementById("data-pracownik-pesel").value;
  const imie = document.getElementById("data-pracownik-imie").value;
  const nazwisko = document.getElementById("data-pracownik-nazwisko").value;
  const data_urodzenia = document.getElementById("data-pracownik-data-urodzenia").value;
  const status_zatrudnienia = document.getElementById("data-pracownik-status-zatrudnienia").value;
  const rola = document.getElementById("data-pracownik-rola-pracownika").value;

  if (!pesel) { alert("Nie podano peselu"); return false; }
  if (!imie) { alert("Nie podano imienia"); return false; }
  if (!nazwisko) { alert("Nie podano nazwiska"); return false; }
  if (!data_urodzenia) { alert("Nie podano daty urodzenia"); return false; }


  const obj = { pesel, imie, nazwisko, data_urodzenia, status_zatrudnienia, rola };
  return obj;
};

let drawModal = () => {
  let content = "";
  content += `
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="dodaj-wypozyczenie" tabindex="-1" role="dialog" aria-labelledby="dodaj-wypozyczenie" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content text-center">
      <div class="modal-header">
        <h5 class="modal-title" id="dodaj-wypozyczenie">Dodaj wypożyczenie</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="dodaj-wypozyczenie-body" class="modal-body">
      <div class="input-group">
      <span class="input-group-addon mr-1" id="basic-addon1">Data wypożyczenia</span>
      <input id="data_with_range_start" class="date-range" type="text"><span class="ml-2 mr-2 date-range-spaces"> do</span>
      <input id="data_with_range_end" class="date-range" type="text">
  </div>
      <p>Filtry</p>
      <button id="szukaj-pojazdow-do-rezerwacji-btn" type="button" class="btn btn-primary btn-lg m-3">Szukaj</button>

      <div id="lista-pojazdow-do-wypozyczenia"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
        <button id="dodaj-wypozyczenie-do-bazy"type="button" class="btn btn-primary">Dodaj</button>
      </div>
    </div>
  </div>
</div>`;

  // dodaj pojazd
  content += `
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="dodaj-pojazd" tabindex="-1" role="dialog" aria-labelledby="dodaj-pojazd" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dodaj-pojazd">Dodaj pojazd</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="dodaj-pojazd-body" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
        <button id="dodaj-pojazd-do-bazy" type="button" class="btn btn-primary">Dodaj</button>
      </div>
    </div>
  </div>
</div>`;
  // dodaj pracownika
  content += `
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="dodaj-pracownika" tabindex="-1" role="dialog" aria-labelledby="dodaj-pracownika" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="dodaj-pracownika">Dodaj pracownika</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Anuluj">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="dodaj-pracownika-body" class="modal-body">
       Dodaj pracownika
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Anuluj</button>
        <button id="dodaj-pracownika-do-bazy" type="button" class="btn btn-primary">Dodaj</button>
      </div>
    </div>
  </div>
</div>`;

  document.getElementById("add-buttons").innerHTML += content;
};

const dodajPojazd = function () {

  
  let content = `
  <div class="input-group">
  <span class="input-group-addon" id="basic-addon1">Nazwa marki</span>
  <input id="data-nazwa-marki" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1" required>
  </div>

  <div class="input-group">
     <span class="input-group-addon" id="basic-addon1">Nazwa modelu</span>
      <input id="data-nazwa-modelu" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
  </div>
  
  <div class="input-group">
     <span class="input-group-addon" id="basic-addon1">Rok produkcji</span>
      <input id="data-rok-prod" type="number" class="form-control" placeholder="" value=2017 aria-describedby="basic-addon1">
  </div>
  
  <div class="input-group">
     <span class="input-group-addon" id="basic-addon1">Przebieg</span>
      <input id="data-przebieg" type="number" class="form-control" placeholder="" aria-describedby="basic-addon1">
  </div>
  
  <div class="input-group">
     <span class="input-group-addon" id="basic-addon1">Nr rejestracyjny</span>
      <input id="data-nr-rej" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
  </div>

  
  <div class="input-group">
     <span class="input-group-addon" id="basic-addon1">Ilosc miejsc</span>
      <input id="data-ilosc-miejsc" type="number" class="form-control" placeholder="" min=1 max=7 value=5 aria-describedby="basic-addon1">
  </div>

  <div class="form-group ">
    <label for="data-typ-nadwozia">Typ nadwozia</label>
    <select class="form-control" id="data-typ-nadwozia">
    ${nadwozie.map((item) => `<option>${item}</option>`).join("")}
  )}

    </select>
  </div>




  <div class="form-group ">
  <label for="data-stan">Stan pojazdu</label>
  <select class="form-control" id="data-stan">
  ${stan.map((item) => `<option>${item}</option>`).join("")}
</select>
</div>

  <div class="form-group ">
  <label for="data-opiekunowie">Opiekun pojazdu</label>
  <select onchange="doAktualizacjiIdPojazduWypozyczenia(this)" class="form-control" id="data-opiekunowie">
${opiekunowie.map((item) => `<option data-target="${item.id}" >${item.imie} ${item.nazwisko}</option>`)}
</select>
</div>
  `;
  document.getElementById("dodaj-pojazd-body").innerHTML = content;
  podajIdOpiekunaDoWypozyczenia = parseInt(document.getElementById('data-opiekunowie').getElementsByTagName('option')[0].getAttribute("data-target"));
};
const doAktualizacjiIdPojazduWypozyczenia = (e) => {
  podajIdOpiekunaDoWypozyczenia = parseInt(e.options[e.selectedIndex].getAttribute('data-target'));
};
const dodajWypozyczenie = function () {

  $(function () {
    $('#data_with_range_start').datetimepicker({
      onShow: function (ct) {
        this.setOptions({
          maxDate: jQuery('#data_with_range_end').val() ? jQuery('#data_with_range_end').val() : false
        })
      },
      timepicker: true,
      lang: 'pl',
      dayOfWeekStart: 1,
    });
    jQuery('#data_with_range_end').datetimepicker({
      onShow: function (ct) {
        if (jQuery('#data_with_range_start').val()) {
          d = new Date(jQuery('#data_with_range_start').val());
          d.setDate(d.getDate() + 1);
        }
        this.setOptions({
          minDate: jQuery('#data_with_range_start').val() ? d : false
        })
      },
      timepicker: true,
      lang: 'pl',
      dayOfWeekStart: 1,
    });
  });


};

const dodajPracownika = function () {

  let content = `
  <div class="input-group">
        <span class="input-group-addon" id="basic-addon1">Pesel</span>
        <input id="data-pracownik-pesel" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
  </div>

        <div class="input-group">
          <span class="input-group-addon" id="basic-addon1">Imię</span>
          <input id="data-pracownik-imie" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
  </div>

          <div class="input-group">
            <span class="input-group-addon" id="basic-addon1">Nazwisko</span>
            <input id="data-pracownik-nazwisko" type="text" class="form-control" placeholder="" aria-describedby="basic-addon1">
</div>


            <div class="form-group row">
              <label for="example-date-input" class="col-2 col-form-label">Data urodzenia</label>
              <div class="col-10">
              
              <input id="data-pracownik-data-urodzenia" type="text" >


  </div>
              </div>

              <div class="form-group ">
                <label for="data-pracownik-status-zatrudnienia">Status zatrudnienia</label>
                <select class="form-control" id="data-pracownik-status-zatrudnienia">
                  ${statusy_zatrudnienia.map((item) => `<option>${item}</option>`).join("")}
                </select>
              </div>


              <div class="form-group ">
                <label for="data-pracownik-rola-pracownika">Rola</label>
                <select class="form-control" id="data-pracownik-rola-pracownika">
                  ${rolePracownikow.map((item) => `<option>${item}</option>`).join("")}
                </select>
              </div>
              `;
  document.getElementById("dodaj-pracownika-body").innerHTML = content;

  $(function () {
    $('#data-pracownik-data-urodzenia').datetimepicker({
      lang: 'pl',
      dayOfWeekStart: 1,
      timepicker: false,
      format: 'Y/m/d'
    })
  });




};
const selectAsActive = (e) => {
  var current = document.getElementsByClassName("aktualny-wybor");
  if (current.length > 0) {
    current[0].className = current[0].className.replace(" aktualny-wybor", "");
  }
  e.target.parentElement.className += " aktualny-wybor";
};
const pokazPojazdyDostepneDoRezerwacji = (data) => {
  if (data.length === 0) { alert('W podanym terminie nie znaleziono pojazdów. Wybierz inny zakres dat.') }
  for (let i = 0; i < data.length; i += 1) {
    let container = document.createElement("TR");
    container.addEventListener("click", (e) => {
      selectAsActive(e);
      wyborPojazduDoRezerwacji = data[i];
    });

    container.setAttribute("id", `container${data[i].id}`);
    // inicjuj perwszy element jako wybor
    if (i === 0) {
      container.setAttribute("class", `cursor-pointer aktualny-wybor`);
      wyborPojazduDoRezerwacji = data[i];
    } else {
      container.setAttribute("class", `cursor-pointer`);
    }
    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].id}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].model.nazwa}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].marka.nazwa}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].nr_rej}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].przebieg}`);
    td.appendChild(text);
    container.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${data[i].model.nadwozie}`);
    td.appendChild(text);
    container.appendChild(td);

    document.getElementById("lista-pojazdow").appendChild(container);
  };

};
const szukajPojazdowDoRezerwacji = () => {
  if (!(document.getElementById("data_with_range_start").value && document.getElementById("data_with_range_end").value)) {
    alert("Wybierz date")
    return;
  }

  // strzal do bazy  z datami po info o pojazdach dostepnych
  let content = `
  <div class="card mb-3">
  <div class="card-header">
    <i class="fa fa-table"></i> Lista dostepnych pojazdów
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Model</th>
            <th>Marka</th>
            <th>Nr rej</th>
            <th>Przebieg</th>
            <th>Nadwozie</th>
           
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Marka</th>
            <th>Model</th>
            <th>Nr rej</th>
            <th>Przebieg</th>
            <th>Nadwozie</th>
           
          </tr>
        </tfoot>
        <tbody id="lista-pojazdow">

        </tbody>
      </table>
    </div>
  </div>
</div>
`;
  // format yyyy-MM-dd-HH:mm
  // moja data 2018/02/15 18:51
  let od = document.getElementById("data_with_range_start").value.replace(' ', '-');
  let koniec = document.getElementById("data_with_range_end").value.replace(' ', '-');
  let urlDostepnychPojazdow = `https://api-bd-car-rent.herokuapp.com/pojazdy/dostepne?od=${od}&do=${koniec}`;

  document.getElementById("lista-pojazdow-do-wypozyczenia").innerHTML = content;

  const proxyurl = "https://cors-anywhere.herokuapp.com/";
  zapytanie(urlDostepnychPojazdow, pokazPojazdyDostepneDoRezerwacji);



  document.getElementById("lista-pojazdow-do-wypozyczenia").innerHTML = content;
  //zapytanie(urlPojazdy, pokazPojazdyDostepneDoRezerwacji);
};