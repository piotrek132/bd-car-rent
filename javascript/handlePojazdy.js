
let podajIdOpiekunaPojazdu = 0;
let temp = null;
document.addEventListener("DOMContentLoaded", (event) => {
  document.getElementsByClassName("container-fluid")[0].classList.add("loader");

  //   var p = new Promise(function(resolve, reject) {  
  //     if (/* condition */) {
  //        resolve(/* value */);  // fulfilled successfully
  //     }
  //     else {
  //        reject(/* reason */);  // error, rejected
  //     }
  //  });


  zapytanie(urlOpiekunow, wczytajOpiekunow);
  zapytanie(urlStatusyPojazdu, wczytajStatusyPojazdow);
  zapytanie(urlNadwozia, wczytajNadwozia);



  zapytanie(urlPojazdy, wczytajRenderujPojazdy);




}); //end of DOMContentLoaded

const DOMtable = document.getElementById("pojazdy");



const editPojazd = (pojazd) => {

};
const renderPojazdy = () => {
  document.getElementsByClassName("container-fluid")[0].classList.remove("loader");
  DOMtable.innerHTML = "";
  let content = "";
  for (let i = 0; i < Pojazdy.length; i++) {
    let td = document.createElement("TH");
    td.setAttribute("id", `td${Pojazdy[i].id}`);

    let btn = document.createElement("BUTTON");
    btn.innerHTML = "EDYTUJ";
    btn.setAttribute("id", `btn${Pojazdy[i].id}`);
    btn.addEventListener("click", (e) => {

      if (e.toElement.innerHTML === "EDYTUJ") {
        podajIdOpiekunaPojazdu = Pojazdy[i].id_opiekuna
        document.getElementById(`opiekun${i}`).disabled = false;
        document.getElementById(`stan${i}`).disabled = false;
        if(localStorage.getItem('rola')!=="MENADŻER") {
          document.getElementById(`opiekun${i}`).disabled = true;
        }



        e.toElement.innerHTML = "ZAPISZ"

      } else {

        // STRZAŁ DO BAZY Z DANYMI
        let elements = e.target.parentElement.parentElement.childNodes;
        temp = elements[6];
        let length = e.target.parentElement.parentElement.childNodes.length;
        let id = elements[0].innerHTML;


        const opiekun = elements[6].getElementsByTagName("select")[0].value;
        const id_opiekuna = parseInt(podajIdOpiekunaPojazdu);
        const status = elements[7].getElementsByTagName("select")[0].value;
        const obj = { id, opiekun, status, id_opiekuna };
        updateDoBazy(obj, urlPojazdy, id);

        // Pojazdy[id-1].model.nazwa = elements[1].getElementsByTagName("input")[0].value;
        // Pojazdy[id-1].marka.nazwa = elements[2].getElementsByTagName("input")[0].value;
        // Pojazdy[id-1].nr_rej = elements[3].getElementsByTagName("input")[0].value;
        Pojazdy[id - 1].opiekun = elements[6].getElementsByTagName("select")[0].value;
        Pojazdy[id - 1].status = elements[7].getElementsByTagName("select")[0].value;
        // Pojazdy[id-1].opis = elements[6].getElementsByTagName("input")[0].value;
        renderPojazdy();




        // USUWAM INPUTA

        // wywołuje render



        e.toElement.innerHTML = "EDYTUJ";
      }


    });
    let pojazd = document.createElement("TR");
    pojazd.setAttribute("id", `pojazd${Pojazdy[i].id}`);

    let text = "";

    td = document.createElement("TD");
    text = document.createTextNode(`${Pojazdy[i].id}`);
    td.appendChild(text);
    pojazd.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pojazdy[i].model.nazwa}`);
    td.appendChild(text);
    pojazd.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pojazdy[i].marka.nazwa}`);
    td.appendChild(text);
    pojazd.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pojazdy[i].nr_rej}`);
    td.appendChild(text);
    pojazd.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pojazdy[i].przebieg}`);
    td.appendChild(text);
    pojazd.appendChild(td);

    td = document.createElement("TD");
    text = document.createTextNode(`${Pojazdy[i].model.nadwozie}`);
    td.appendChild(text);
    pojazd.appendChild(td);


    td = document.createElement("TD");
    idd = "opiekun" + i;
    text = `
    <div class="form-group">
      <select onchange="doAktualizacjiIdOpiekuna(this)" class="form-control" id=${idd}>
        ${opiekunowie.map((item) => `<option data-target=${item.id}>${item.imie} ${item.nazwisko}</option>`).join("")}
      </select>
    </div>`;
    td.innerHTML = text;
    pojazd.appendChild(td);

    td = document.createElement("TD");
    idd = "stan" + i;
    text = `
    <div class="form-group">
      <select onchange=alertZmianyStatusuPojazdu(this,"${Pojazdy[i].status}") class="form-control" id=${idd}>
        ${stan.map((item) => `<option value=${item}>${item.replace("_", " ")}</option>`).join("")}
      </select>
    </div>`;
    td.innerHTML = text;
    pojazd.appendChild(td);


    td = document.createElement("TD");
    td.setAttribute("id", `edit-btn`);
    let check = parseInt(localStorage.getItem("id_pracownika"));
    if(localStorage.getItem('rola') ==='MENADŻER' || Pojazdy[i].status!=="ZŁOMOWANY" && (check === Pojazdy[i].id_opiekuna)){
      td.appendChild(btn);
     }
    pojazd.appendChild(td);


    DOMtable.appendChild(pojazd);


    document.getElementById(`opiekun${i}`).value = Pojazdy[i].opiekun;
    document.getElementById(`stan${i}`).value = Pojazdy[i].status;
    document.getElementById(`opiekun${i}`).disabled = true;
    document.getElementById(`stan${i}`).disabled = true;
    
  
    
   



  }
  blokada();
};

const doAktualizacjiIdOpiekuna = (e) => {
  podajIdOpiekunaPojazdu = e.options[e.selectedIndex].getAttribute('data-target');
};
const alertZmianyStatusuPojazdu = (e, status) => {
  const value = e.options[e.selectedIndex].value;

  if (value === "W_NAPRAWIE") {
    alert("Uwaga. Wybierając ten status wszystkie wypożyczenia danego pojazdu zostaną usunięte.");
  } else if (value === 'ZŁOMOWANY') {
    alert("Uwaga. Wybierając ten status wszystkie wypożyczenia danego pojazdu zostaną usunięte oraz jego opieka zostanie usunięta.");
  }
};
