
//const przedrostek = "https://api-bd.herokuapp.com";
//const urlOpiekunow = przedrostek + "/pracownicy?rola=opiekun";
const przedrostek = "https://api-bd-car-rent.herokuapp.com";
const urlOpiekunow = przedrostek + "/pracownicy/opiekuni";

const urlPojazdy = przedrostek + "/pojazdy";
const urlPracownicy = przedrostek + "/pracownicy";
const urlWypozyczenia = przedrostek + "/wypozyczenia";

const urlStatusyPojazdu = przedrostek + "/statusyPojazdu";
const urlStatusyZatrudnienia = przedrostek + "/statusyZatrudnienia";
const urlStatusyWypozyczenia = przedrostek + "/statusyWypozyczenia";
const urlRolePracownikow = przedrostek + "/rolePracownikow";
const urlNadwozia = przedrostek + "/nadwozia";
const urlWydatkiRoczne = przedrostek + "/wydatkiRoczne";
const urlWydatkiMiesieczne = przedrostek + "/wydatkiMiesieczne";
const urlWszystkieMiesieczneWydatki = przedrostek + "/wszystkieWydatkiWMiesiacu";
const urlWydatkiKierowcy = przedrostek + "/wydatkiKierowcy";
const urlPojazdySerwisowane = przedrostek + "/pojazdy/wNaprawie";
const urlPojazdySprawne = przedrostek + "/pojazdy/sprawne";


const urlCzynnosciSerwisowych = przedrostek + "/czynnosciSerwisowe";
const urlListaCzynnosciSerwisowych = przedrostek + "/listaCzynnosciSerwisowych";
const urlCzynnosciEksploatacyjnych = przedrostek + "/czynnosciEksploatacyjne";
const urlListaCzynnosciEksploatacyjnych = przedrostek + "/listaCzynnosciEksploatacyjnych";

const zapytanie = (url, givenFunc,id="") => {
  console.log(url + (id ==="" ? id : "/" + id), '<- zapytanie GET');
  fetch(url + (id ==="" ? id : "/" + id), { method: 'get' })
    .then((resp) => resp.json()) // Transform the data into json
    .then(function (data) {
      if(typeof givenFunc!==null){
      givenFunc(data);
      }
    })
};

const wczytajRenderujPojazdy = (data) => {
  Pojazdy = data;
  setTimeout(() => {
    renderPojazdy();
  }, 250);

};
const wczytajRenderujPracownikow = (data) => {
  Pracownicy = data;
  setTimeout(() => {
    renderPracownicy();
  }, 250);
};
const wczytajOpiekunow = (data) => {
  opiekunowie = data;
};
const wczytajPracownikowChart = (data) => {
  Pracownicy = data;
};
const wczytajWypozyczenia= (data) => {
  Wypozyczenia = data;
  setTimeout(() => {
    renderWypozyczenia();
  }, 250);
};
const wczytajPracownikow =wczytajRenderujPracownikow;

const wczytajStatusyPojazdow = (data) => {
  stan = data;
};
const wczytajNadwozia = (data) => {
  nadwozie = data;
};
const wczytajStatusyZatrudnienia = (data) => {
  statusy_zatrudnienia = data;
};
const wczytajStatusyWypozyczenia = (data) => {
  statusy_wypozyczenia = data;
};
const wczytajRolePracownikow = (data) => {
  rolePracownikow = data;
};
const wczytajListeCzynnosciEksploatacyjnych = (data) => {
  listaCzynnosciEksploatacyjnych = data;
};
const wczytajListeCzynnosciSerwisowych = (data) => {
  listaCzynnosciSerwisowych = data;
};


const postDoBazy = (url, data) => {
 console.log(url,'<= strzelam POST');
 console.log(data,'dane strzelone POSTem')
fetch(url, {
  method: 'POST', // or 'PUT'
  body: typeof data === "string" ?data :JSON.stringify(data), 
  headers: new Headers({
    'Content-Type': 'application/json'
  })
}).then(res => res.json())
.catch(error => console.error('Error:', error))
.then(response => console.log('Success:'));
};

const dodajPojazdDoBazy = (data, url) => {
  console.log(url,'<= strzelam POST');
  console.log(data,'dane strzelone POSTem')
 fetch(url, {
   method: 'POST', // or 'PUT'
   body: typeof data === "string" ?data :JSON.stringify(data), 
   headers: new Headers({
     'Content-Type': 'application/json'
   })
 }).then(res => res.json())
 .catch(error => console.error('Error:', error))
 .then(response => console.log('Success:'));
 };
 
const dodajPracownikaDoBazy = (data, url) => {
  console.log(url,'<= strzelam POST');
  console.log(data,'dane strzelone POSTem');
   fetch(url, {
   method: 'POST', // or 'PUT'
   body: typeof data === "string" ?data :JSON.stringify(data), 
   headers: new Headers({
     'Content-Type': 'application/json'
   })
 }).then(res => res.json())
 .catch(error => console.error('Error:', error))
 .then(response => console.log('Success:'));
 };

const updateDoBazy = (data, url, identification="") => {
  console.log(url,'<= strzelam PATCH');
  console.log(data,'dane strzelone PATCH')
 fetch(url + (identification ==="" ? identification : "/" + identification), {
   method: "PATCH", // or 'PUT'
   body: typeof data === "string" ?data :JSON.stringify(data), 
   headers: new Headers({
     'Content-Type': 'application/json'
   })
 }).then(res => res.json())
 .catch(error => console.error('Error:', error))
 .then(response => console.log('Success:'));
 };
 

const usunZBazy = (url, identification="") => {

  if(!identification){alert("brak id");return;}
  console.log(url + (identification ==="" ? identification : "/" + identification),'<= strzelam DELETE');
 fetch(url + (identification ==="" ? identification : "/" + identification), {
   method: "DELETE",
 }).then(res => res.json())
 .catch(error => console.error('Error:', error))
 .then(response => console.log('Success:'));
 };
 